<?php

namespace Regis\Http\Controllers\Api;

use Illuminate\Http\Request;
use Regis\Http\Controllers\Controller;
use Regis\Http\Requests\ProductCategoryRequest;
use Regis\Http\Resources\ProductCategoryResource;
use Regis\Models\Category;
use Regis\Models\Product;

class ProductCategoryController extends Controller
{
    /**
     * Lista as categorias de um produto
     * @param Product $product
     * @return mixed
     */
    public function index(Product $product)
    {
        return new ProductCategoryResource($product);
    }

    /**
     * Vincula categorias a um produto
     * @param ProductCategoryRequest $request
     * @param Product $product
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(ProductCategoryRequest $request, Product $product)
    {
        $changed = $product->categories()->sync($request->categories);
        $categoriesAttachedId = $changed['attached'];
        $categories = Category::whereIn('id', $categoriesAttachedId)->get();

        return $categories->count() ? response()->json(new ProductCategoryResource($product), 201) : [];
    }

    /**
     * Desvincula uma categoria de um produto
     * @param Product $product
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product, Category $category)
    {
        $product->categories()->detach($category->id);
        return response()->json([], 204);
    }
}
