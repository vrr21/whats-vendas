<?php

namespace Regis\Http\Controllers\Api;

use Regis\Http\Controllers\Controller;
use Regis\Http\Requests\ProductInputRequest;
use Regis\Models\Product;
use Regis\Models\ProductInput;
use Illuminate\Http\Request;

class ProductInputController extends Controller
{

    public function index()
    {
        return Product::all();
    }

    public function store(ProductInputRequest $request)
    {
        $input = ProductInput::create($request->all());

        $product = Product::find($input->product_id);
        $product->stock += $input->amount;
        $product->save();

        return $input;
    }

    public function show(ProductInput $input)
    {
        return $input;
    }
}
