<?php

use Faker\Generator as Faker;

$factory->define(Regis\Models\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->colorName
    ];
});
