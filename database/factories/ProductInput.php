<?php

use Faker\Generator as Faker;

$factory->define(\Regis\Models\ProductInput::class, function (Faker $faker) {
    return [
        'amount' => $faker->numberBetween(1, 10)
    ];
});
