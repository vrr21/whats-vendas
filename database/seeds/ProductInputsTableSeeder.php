<?php

use Illuminate\Database\Seeder;
use Regis\Models\Product;

class ProductInputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        factory(\Regis\Models\ProductInput::class, 10)->make()->each(function($input) use ($products) {
            $product = $products->random();
            $input->product_id = $product->id;
            $input->save();

            $product->stock += $input->amount;
            $product->save();
        });
    }
}
